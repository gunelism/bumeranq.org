@extends('layouts.web')

@section('title','Profil')

@section('content')

    <div id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-left">Profilim</h1>
                </div>
                <div class="col-lg-9">
                    <ul class="nav nav-tabs profil-navigatior">
                        <li {{Request::is('profil/1') ? "class=active" : ''}}><a data-toggle="tab" href="#profil-view">Profil görünüşü</a></li>
                        <li {{Request::is('profil/123') ? "class=active" : ''}}><a data-toggle="tab" href="#profil-mesajlarim">Mesajlar</a></li>
                        <li {{Request::is('my-posts/0') ? "class=active" : ''}}><a data-toggle="tab" href="#profil-isteklerim">İstəklərim</a></li>
                        <li {{Request::is('my-posts/1') ? "class=active" : ''}}><a data-toggle="tab" href="#profil-desteklerim">Dəstəklərim</a></li>
                        <li {{Request::is('Destekolduqlarim') ? "class=active" : ''}}><a data-toggle="tab" href="#profil-destekolduqlarim">Dəstək olduqlarım</a></li>
                        <li {{Request::is('Istekverdiklerim') ? " class=active" : ''}}><a data-toggle="tab" href="#profil-istekverdiklerim">İstək verdiklərim</a></li>
                        <li {{Request::is('notifications/1') ? " class=active" : ''}}><a data-toggle="tab" href="#profil-notification">Bildirişlər</a></li>
                        <li {{Request::is('settings/1') ? " class=active" : ''}}><a data-toggle="tab" href="#profil-settings">Tənzimləmələr</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <section id="profil">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-content">
                        {{-- <================== PROFIL PART==================> --}}

                        <div id="profil-view" class="tab-pane fade in {{Request::is('profil/1') ? "active" : ''}}">
                            <div class="col-lg-3 col-md-2 col-sm-2 col-xs-4 col-lg-offset-0 col-md-offset-0 col-sm-offset-0 col-xs-offset-4 padding0 profil-avatar">
                                <img src="{{url('/image/'.Auth::user()->avatar)}}" alt="Avatar">
                            </div>
                            <div class="col-lg-9 col-sm-9 col-xs-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-0 col-xs-offset-3 profil-name">
                                <h2>{{Auth::user()->name}}</h2>
                                <a href="{{url('/settings/1')}}"><h2 class="pull-right"><i class="fa fa-pencil-square-o"></i></h2></a>
                                <hr>
                            </div>
                            <div class="col-lg-9 col-sm-9 col-xs-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-0 col-xs-offset-3 profil-phone">
                                <p><i class="fa fa-phone"></i> {{Auth::user()->phone}}</p>
                            </div>
                            <div class="col-lg-9 col-sm-9 col-sm-offset-2 col-xs-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-0 col-xs-offset-3 profil-email">
                                <p><i class="fa fa-envelope"></i> {{Auth::user()->email}}</p>
                            </div>
                            <div class="col-lg-9 col-md-9 col-md-offset-2 col-sm-9 col-sm-offset-2 col-xs-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-0 col-xs-offset-3 profil-address">
                                <p><i class="fa fa-map-marker"></i> {{Auth::user()->city->city_name}}</p>
                            </div>
                        </div>
                        {{-- <================== PROFIL PART END ==================> --}}


                        {{-- <================== ISTEKLERIM PART==================> --}}

                        <div id="profil-isteklerim" class="tab-pane fade in{{Request::is('my-posts/0') ? " active" : ''}}">
                            @if (Session::has('istek_edited'))
                                <div class="alert alert-success" role="alert">{{Session::get('istek_edited')}}</div>
                            @endif
                             <div class="table-responsive">
                            {{--@if ($istek == 0)--}}
                                {{--<h1>İstəyiniz yoxdur</h1>--}}
                            {{--@else--}}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Dərc olunub?</th>
                                        <th>Bitmə vaxtı</th>
                                        <th>Başlıq</th>
                                        <th>Təsvir</th>
                                        <th>Şəkil</th>
                                        <th>Yenilə & Sil</th>
                                    </tr>
                                    </thead>
                                    {{--@endif--}}
                                    @foreach (Auth::user()->posts as $post)
                                        <tbody>
                                        @if ($post->user_id == Auth::user()->id && $post->post_type == '0')
                                            <tr>
                                                @php
                                                    $derc_status = 'Dərc olunmayıb';
                                                    $derc_icon = 'fa fa-times-circle-o fa-2x';
                                                    if ($post->is_active==1) {
                                                         $derc_status = " Dərc olunub";
                                                         $derc_icon = 'fa fa-check-circle-o fa-2x';
                                                    }
                                                @endphp
                                                <td class="profil-status" title="{{$derc_status}}"><i class="{{$derc_icon}}"></i></td>
                                                <td>{{$post->deadline}}</td>
                                                <td>{{$post->title}}</td>
                                                <td class="profil-subText">{{substr($post->about,0,100)}}...</td>
                                                <td class="profil-photo"><img src="{{url('/image/'.$post->photos[0]->photo_name)}}" class="img-responsive" alt="News image"></td>
                                                <td class="profil-action">
                                                    <a href="{{url('/post-edit/'.$post->post_type.'/'.$post->slug)}}" class="btn action-edit"><i class="fa fa-pencil-square"></i></a>
                                                    <a href="#" data-toggle="modal" data-target="#{{$post->id}}" class="btn action-delete"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                         {{--For Delete Button Modal--}}
                                        <div id="{{$post->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title text-center" id="myModalLabel">Əminsinizmi?</h4>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <button class="btn btn-primary" type="button" class="close" data-dismiss="modal" aria-label="Close">Xeyir
                                                        </button>
                                                        <a href="{{url('/post-delete/'.$post->id)}}" class="btn btn-danger">Bəli</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         {{--For Delete Button Modal END--}}
                                        @endif
                                    @endforeach
                                </table>
                                 </div>
                        </div>
                        {{-- <================== Mesajlarim PART ==================> --}}
                        <div id="profil-mesajlarim" class="tab-pane fade in{{Request::is('profil/123') ? " active" : ''}}">
                            <div id="chat" post="0" sender="0" user="{{Auth::user()->id}}" class="dsp_none messenger">
                                <div class="users-list">
                                    <div class="chat-header">
                                        <h5 class="chat-header-name">istifadəçilər</h5>
                                    </div>
                                    <div class="chat-body">
                                        <input type="text" class="form-control search-user" placeholder="Axtar">
                                        <ul class="list-group chat-body-message list-unstyled"  >
                                            @if(isset($messages[0]))
                                                @foreach($messages as $message)
                                                    <li id="{{$message->post_id}}" class="pull-left users">
                                                        <img src="/image/{{$message->sender->avatar}}" class="chat-message-img user-avatar" alt="{{$message->sender->id}}">
                                                        <p class="user-desc">
                                                            <strong>{{$message->sender->name}} <br></strong>
                                                            <span>{{substr($message->message,0,22)}} ...</span>
                                                        </p>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li  class="pull-left users">
                                                    <p class="user-desc">İstifadəçi yoxdur
                                                    </p>
                                                </li>
                                            @endif

                                        </ul>
                                    </div>
                                </div>
                                <div class="chat-header">
                                    <h5 class="chat-header-name receiver_name"></h5>
                                </div>
                                <div class="chat-body">
                                    <ul class="list-group chat-body-message chat-messenger-body list-unstyled">
                                    </ul>
                                </div>
                                <div class="chat-footer mesenger-footer">
                                    <form id="notification_messenger">
                                        <div class="col-lg-10 padding0">
                                            <input type="text" class="form-control chat-footer-input" placeholder="Mesajınız">
                                        </div>

                                        <div class="col-lg-2 padding0">
                                            <button type="submit" name="button" class="btn chat-footer-btn"><i class="fa fa-paper-plane-o"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- <================== DESTEKLERIM PART ==================> --}}

                        <div id="profil-desteklerim" class="tab-pane fade in{{Request::is('my-posts/1') ? " active" : ''}}">
                            {{-- <div class="table-responsive"> --}}
                            <table class="table">
                                {{--@if ($destek == null)--}}
                                {{--<h1>Dəstəyiniz yoxdur</h1>--}}
                                {{--@else--}}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Dərc olunub?</th>
                                        <th>Bitmə vaxtı</th>
                                        <th>Başlıq</th>
                                        <th>Təsvir</th>
                                        <th>Şəkil</th>
                                        <th>Yenilə & Sil</th>
                                    </tr>
                                    </thead>
                                    {{--@endif--}}
                                    @foreach (Auth::user()->posts as $post)
                                        <tbody>
                                        <tr>
                                            @if ($post->user_id == Auth::user()->id && $post->post_type == '1')
                                                @php
                                                    $derc_status = 'Dərc olunmayıb';
                                                    $derc_icon = 'fa fa-times-circle-o fa-2x';
                                                    if ($post->is_active==1) {
                                                        $derc_status = " Dərc olunub";
                                                        $derc_icon = 'fa fa-check-circle-o fa-2x';
                                                      }
                                                @endphp
                                                <td class="profil-status" title="{{$derc_status}}"><i class="{{$derc_icon}}"></i></td>
                                                <td>{{$post->deadline}}</td>
                                                <td>{{$post->title}}</td>
                                                <td class="profil-subText">{{substr($post->about,0,100)}}...</td>
                                                <td class="profil-photo"><img src="{{url('/image/'.$post->photos[0]->photo_name)}}" class="img-responsivse" alt="News image"></td>
                                                <td class="profil-action">
                                                    <a href="{{url('/post-edit/'.$post->post_type.'/'.$post->slug)}}" class="btn action-edit"><i class="fa fa-pencil-square"></i></a>
                                                    <a href="#" data-toggle="modal" data-target="#{{$post->id}}" class="btn action-delete"><i class="fa fa-trash"></i></a>
                                                </td>
                                        </tr>
                                        </tbody>
                                        {{--For Delete Button Modal--}}
                                        <div id="{{$post->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title text-center" id="myModalLabel">Əminsinizmi?</h4>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <button class="btn btn-primary" type="button" class="close" data-dismiss="modal" aria-label="Close">Xeyir
                                                        </button>
                                                        <a href="{{url('/post-delete/'.$post->id)}}" class="btn btn-danger">Bəli</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--For Delete Button Modal END--}}
                                        @endif
                                    @endforeach
                                </table>
                            {{-- </div> --}}
                        </div>


                        {{-- <================== DESTEK OLDUQLARIM PART ==================> --}}
                        {{-- {{dd($help[0]->type_id)}} --}}
                        <div id="profil-destekolduqlarim" class="tab-pane fade in{{Request::is('Tekliflerim') ? " active" : ''}}">
                            {{--@if(count($help)!=0)--}}
                                {{--@if($help[0]->type_id==2)--}}
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Qəbul olunub?</th>
                                            <th>Başlıq</th>
                                            <th>Təsvir</th>
                                            <th>Şəkil</th>
                                        </tr>
                                        </thead>

                                        @foreach (Auth::user()->user_post as $helps)
                                            <tbody>
                                            @if ($helps->post->post_type == 0)
                                                <tr>
                                                    @php
                                                        $status = 'İmtina edilib';
                                                        $status_icon = 'fa fa-times-circle-o fa-2x';
                                                        if ($helps->accepted == 1)
                                                         {
                                                            $status = 'Qəbul olunub';
                                                            $status_icon = 'fa fa-check-circle-o fa-2x';
                                                          }
                                                    @endphp
                                                    <td class="profil-status" title="{{$status}}"><i class="{{$status_icon}}"></i></td>
                                                    <td>{{$helps->post->title}}</td>
                                                    <td class="profil-subText">{{substr($helps->post->about,0,100)}}...</td>
                                                    <td class="profil-photo"><img src="{{url('/image/'.$helps->post->photos[0]->photo_name)}}" class="img-responsive" alt="News image"></td>
                                                </tr>
                                            </tbody>
                                            @endif
                                        @endforeach
                                    </table>
                                {{--@else--}}
                                    {{--<h1>Heç bir istəyə dəstək olmamısız</h1>--}}


                                {{--@endif--}}
                            {{--@else--}}
                                {{--<h1>Heç bir istəyə dəstək olmamısız</h1>--}}

                            {{--@endif--}}

                            {{-- </div> --}}
                        </div>


                        {{-- <================== TEKLIFLERIM PART ==================> --}}

                        <div id="profil-istekverdiklerim" class="tab-pane fade in{{Request::is('Tekliflerim') ? " active" : ''}}">
                            {{--@if(count($help)!=0)--}}
                            {{--@php--}}
                            {{--dd(Auth::user()->user_post[0]->post->description);--}}
                            {{--@endphp--}}
                                {{--@if($help[0]->type_id==1)--}}
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Qəbul olunub?</th>
                                            <th>Başlıq</th>
                                            <th>Təsvir</th>
                                            <th>Şəkil</th>
                                        </tr>
                                        </thead>
                                        @foreach (Auth::user()->user_post as $helps)
                                            <tbody>
                                            @if ($helps->post->post_type == 1)
                                                <tr>
                                                    @php
                                                        $status = 'İmtina edilib';
                                                        $status_icon = ' fa fa-times-circle-o fa-2x';
                                                        if ($helps->accepted == 1)
                                                         {
                                                            $status = 'Qəbul olunub';
                                                            $status_icon = 'fa fa-check-circle-o fa-2x';
                                                          }
                                                    @endphp
                                                    <td class="profil-status" title="{{$status}}"><i class="{{$status_icon}}"></i></td>
                                                    <td>{{$helps->post->title}}</td>
                                                    <td class="profil-subText">{{substr($helps->post->about,0,100)}}...</td>
                                                    <td class="profil-photo"><img src="{{url('/image/'.$helps->post->photos[0]->photo_name)}}" class="img-responsive" style="height: 50px;" alt="News image"></td>
                                                </tr>
                                            </tbody>

                                            @endif
                                        @endforeach
                                    </table>
                                {{--@else--}}
                                    {{--<h1>Heç bir dəstəyə istək verməmisiniz</h1>--}}


                                {{--@endif--}}
                            {{--@else--}}
                                {{--<h1>Heç bir dəstəyə istək verməmisiniz</h1>--}}

                            {{--@endif--}}

                        </div>


                        {{-- <================== NOTIFICATION PART==================> --}}

                        <div id="profil-notification" class="tab-pane fade in {{Request::is('notifications/1') ? " active" : ''}}">
                             @if (!isset($userPost))
                              <h1>Bildirişiniz yoxdur</h1>
                            @else
                            @foreach($userPost as $notification_send)
                                @if($notification_send->post_user_id==Auth::user()->id)
                                    <div class="col-lg-12 padding0 notification-block">
                                        <div class="col-lg-2">
                                            <img src="{{url('/image/'.$notification_send->user->avatar)}}">
                                        </div>
                                        <div class="col-lg-9">
                                            <h4 class="profil-notification-title">
                                                @if($notification_send->post->post_type==0)
                                                    <span class="special-istek">{{$notification_send->user->name}}</span> adlı istifadəçi istəyinizə dəstək vermək istəyir !
                                                @elseif($notification_send->post->post_type==1)
                                                    <span class="special-destek">{{$notification_send->user->name}}</span> adlı istifadəçi dəstəyinizdən yararlanmaq istəyir !
                                                @endif
                                            </h4>
                                            <p class="profil-notification-desc">{{$notification_send->description}}</p>
                                            <div class="col-lg-1 col-lg-offset-11 col-md-offset-10  col-sm-offset-9 col-xs-offset-4 padding0">
                                                <p class="profil-notification-full"><a href="{{url('/notification/'.$notification_send->id)}}" class="btn zaa">Tam müraciətə bax<i class="fa fa-angle-double-right"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                            @foreach($userPost as $notification_accepted)
                                @if($notification_accepted->user_id == Auth::user()->id && $notification_accepted->accepted == 1)

                                    <div class="col-lg-12 padding0 notification-block">
                                        <div class="col-lg-2">
                                            <img src="{{url('/image/'.$notification_accepted->post->user->avatar)}}">
                                        </div>
                                        <div class="col-lg-9">
                                            <h4 class="profil-notification-title">
                                                @if($notification_accepted->post->post_type==0)
                                                    <span class="special-istek">{{$notification_accepted->post->user->name}}</span>  adlı istifadəçi dəstəyinizi qəbul etdi !
                                                @endif
                                                @if($notification_accepted->post->post_type==1)
                                                    <span class="special-destek">{{$notification_accepted->post->user->name}}</span>  adlı istifadəçi istəyinizi qəbul etdi !
                                                @endif
                                            </h4>
                                            <p class="profil-notification-desc">{{$notification_accepted->description}}</p>
                                            <p class="profil-notification-full pull-right"><a href="{{url('/notification/'.$notification_accepted->id)}}" class="btn zaa">Tam müraciətə bax<i class="fa fa-angle-double-right"></i></a></p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            @endif
                        </div>
                        {{-- <================== NOTIFICATION PART END ==================> --}}


                        {{-- <================== MESSAGE PART  ==================> --}}
                        {{-- <div id="profil-ismariclar" class="tab-pane fade in {{Request::is('Ismarıclar') ? " active" : ''}}">
                        </div> --}}


                        {{-- <================== TENZIMLEMELER PART==================> --}}
                        <div id="profil-settings" class="tab-pane fade in {{Request::is('settings/1') ? " active" : ''}}">
                            @if (Session::has('imageerror'))
                                <div class="alert alert-danger" role="alert">{{Session::get('imageerror')}}</div>
                            @endif
                            @if (Session::has('settings'))
                                <div class="alert alert-success" role="alert">{{Session::get('settings')}}</div>
                            @endif
                            @if ($errors->has('name') || $errors->has('phone') || $errors->has('avatar'))
                                <span class="help-block">
                                <div class="alert alert-danger"><p>Ulduz ilə işarəli xanaları boş saxlamayın.</p></div>
                            </span>
                            @endif
                            <div id="ErrorImage" ></div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0 profil-avatar">
                                <img src="{{url('/image/'.Auth::user()->avatar)}}" class="shoUploadedImg center-block" alt="Avatar">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <form action="{{url('/profile-update')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <p>
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="email">Ad,Soyad<SPAN> *</SPAN></label>
                                        <input class="form-control" type="text" name="name" value="{{Auth::user()->name}}">
                                    </div>
                                    </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <p>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="email">Əlaqə nömrəsi<SPAN> *</SPAN></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            +994
                                            <select id="operator-numbers" name="operator">
                                                <option {{substr(Auth::user()->phone,4,2) == '55' ? 'selected' : '' }}>55</option>
                                                <option {{substr(Auth::user()->phone,4,2) == '51' ? 'selected' : '' }}>51</option>
                                                <option {{substr(Auth::user()->phone,4,2) == '50' ? 'selected' : '' }}>50</option>
                                                <option {{substr(Auth::user()->phone,4,2) == '70' ? 'selected' : '' }}>70</option>
                                                <option {{substr(Auth::user()->phone,4,2) == '77' ? 'selected' : '' }}>77</option>
                                            </select>
                                        </div>
                                        <input type="text" class="form-control" name="phone" maxlength="7" value="{{substr(Auth::user()->phone,6)}}">
                                    </div>
                                </div>
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <p>
                                <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">

                                    <label for="file">Şəkil:</label>
                                    <a class="forImg form-control btn btn-default">Şəkil Seç</a>
                                    <input class="imgInput form-control hidden" type="file" name="avatar" value="{{Auth::user()->avatar}}">
                                </div>
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <p>
                                <div class="form-group">
                                    <label for="email">Ünvan:</label>
                                    <select id="CitySelectOption" class="form-control" name="city">
                                        @foreach($cities as $city)
                                            <option {{( Auth::user()->city_id == $city->id) ? 'selected':''}} value="{{$city->id}}">{{$city->city_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                </p>
                            </div>
                            <div class="col-lg-12">
                                <p>
                                <div class="form-group pull-right">
                                    <input class="btn" type="submit" name="submit" value="Göndər">
                                </div>
                                </p>
                                </form>
                            </div>
                        </div>
                        {{-- <================== TENZIMLEMELER END==================> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection