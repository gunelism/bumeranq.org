<?php

namespace App\Http\Controllers\Social;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class FacebookController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        // dd(Socialite::driver('facebook'));
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        $users = Socialite::driver('facebook')->user();
        $user = $users->getName();
        $myEmail = $users->getEmail();

        $request->session()->flash('user',$user);
        $request->session()->flash('email',$myEmail);
        return redirect('/register');

    }
}
