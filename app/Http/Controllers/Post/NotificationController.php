<?php

namespace App\Http\Controllers\Post;


use App\Events\CreateNotificationEvent;
use App\Http\Requests\NotificationRequest;
use App\Mail\EmailNotification;
use App\Message;
use App\Post;
use App\UserPost;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;

class NotificationController extends Controller
{
    public function add_notification(NotificationRequest $request,$type,$id,UserPost $userPost)
    {
        $post = Post::find($id);
        if ($this->ChechPostType($type) || $this->ChechData($post))
        {
            $userPost->post_id = $id;
            $userPost->user_id = Auth::user()->id;
            $userPost->post_user_id = $post->user_id;
            $userPost->read = 0;
            $userPost->accepted = 0;
            $userPost->description = $request->description;
            $userPost->save();
            event(new CreateNotificationEvent($userPost));
            $email = new EmailNotification(['user' => Auth::user()->name,'post_user' => $post->user->name,'post' => $post->title,'post_type' => $post->post_type]);
            Mail::to($post->user->email)->send($email);
            if ($post->post_type == 0)
            {
                $request->session()->flash('description_destek' , "Dəstəyiniz uğurla  göndərildi. Qəbul olunduğu zaman sizə bildiriş göndəriləcək ");
            }
            else
            {
                $request->session()->flash('description_istek' , "İstəyiniz uğurla  göndərildi. Qəbul olunduğu zaman sizə bildiriş göndəriləcək ");
            }
            return back();
        }
        else
        {
            return view('errors.503');
        }
    }

    public function get_notifications(Request $request,$count)
    {
        $userPosts = UserPost::where('post_user_id',Auth::user()->id)->orWhere('user_id',Auth::user()->id)->orderBy('created_at','desc')->limit(5)->get();
        if ($count !== 0)
        {
            foreach ($userPosts as $userPost) {
                $userPost->read = 0;
                $userPost->update();
            }
        }
        if ($request->ajax()) {
            if ($this->CheckCollectionData($userPosts)) {
                return response()->json($userPosts);
            } else {
                return 'false';
            }
        }
    }

    public function notification($id)
    {
        $notification = UserPost::find($id);
        if ($this->ChechData($notification))
        {
            if ( $notification->post_user_id == Auth::user()->id || $notification->user_id == Auth::user()->id)
            {
                return view('web.notification',compact('notification'));
            }
            else
            {
                return view('errors.404');
            }
        }
        else
        {
            return view('errors.503');
        }
    }

    public function notification_accepted($id)
    {
        $notification = UserPost::where([['id',$id],['user_id',Auth::user()->id],['accepted','1']])->first();
        if ($this->ChechData($notification))
        {
            return view('web.notification_accepted',compact('notification'));
        }
        else
        {
            return view('errors.503');
        }
    }

    public function notification_refusal($id)
    {
        $userPost = UserPost::find($id);
        if ($this->ChechData($userPost) && $userPost->post_user_id == Auth::user()->id)
        {
            $delete_chats = Message::where('sender_id', '=', $userPost->user_id)->where('receiver_id', '=', Auth::user()->id )->get();
            $delete_messages = Message::where('sender_id', '=', Auth::user()->id )->where('receiver_id', '=', $userPost->user_id )->get();
            if ($delete_messages)
            {
                foreach ($delete_messages as $delete_message)
                {
                    $delete_message->delete();
                }
            }
            if ($delete_chats)
            {
                foreach ($delete_chats as $delete_chat)
                {
                    $delete_chat->delete();
                }
            }
            $userPost->accepted=2;
            $userPost->update();
            return back();
        }
        else
        {
            return view('errors.503');
        }
    }

    public function accept($id,Request $request)
    {
        $notification = UserPost::where([['id',$id],['post_user_id',Auth::user()->id]])->first();
        if ($this->ChechData($notification))
        {
            $notification->accepted = 1;
            $notification->update();
            $request->session()->flash('accept', 'İsmarıcınız müvəffəqiyyətlə göndərildi.');
            return back();
        }
        else
        {
            return view('errors.503');
        }
    }
}