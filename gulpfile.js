const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
    mix.sass('style.scss')
mix.styles(['../../../public/css/iziToast.css','../../../public/css/style.css'], 'public/css/bumeranq.css')
    .scripts(['main.js','notification.js','message.js','messenger.js','iziToast.js'], 'public/js/all.js')
    .scripts(['jquery-ui.js','InfoBubble.js','AjaxSearchMap.js'], 'public/js/customMap.js');
});

