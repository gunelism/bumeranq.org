<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PostUpdateRequest extends FormRequest
{

    public function __construct()
    {
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'about' => 'required',
            'location' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'type' => 'required',
            'deadline' => 'required|after:tomorrow'
        ];
    }

    public function messages()
    {
        return [
            'deadline.required' => 'Elanın bitmə tarixini daxil edin.',
            'deadline.after' => 'Elanın bitmə tarixi 1 gündən az ola bilməz.'
        ];
    }
}
