<?php

namespace App\Http\Controllers\Post;

use App\Http\Requests\PostUpdateRequest;
use App\Mail\EmailNewPost;
use App\Post;
use App\PostImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostAddRequest;
use Illuminate\Support\Facades\Mail;

class PostController extends Controller
{
    public function create_post($type, PostAddRequest $request)
    {
        if ($this->ChechPostType($type))
        {
            $post_fields = $this->FilterRequestForPostFields($request,$type);
            $photo_id = Auth::user()->posts()->create($post_fields)->photos();
            $files = $request->file('image');
            foreach ($files as $file)
            {
                $image_name =  rand(1,99999).'-'.$post_fields['slug'].'.'.$file->getClientOriginalExtension();
                $file->move(public_path('image'),$image_name);
                $images = new PostImage;
                $images->photo_name = $image_name;
                $photo_id->save($images);
            }
            $request->session()->flash('is_active', 'İstəyiniz uğurla  əlavə olundu və yoxlamadan keçəndən sonra dərc olunacaq.');
            $moderators = User::where('is_admin','=',1)->get();
            foreach ($moderators as $moderator)
            {
                $email = new EmailNewPost(['moderator' => $moderator->name,'user' => Auth::user()->name,'post_title' =>$post_fields['title'],'post_type' => $post_fields['post_type']]);
                Mail::to($moderator->email)->send($email);
            }
            return back();
        }
        else
        {
            return view('errors.503');
        }
    }

    public function update_post($type,$id,PostUpdateRequest $request)
    {
        $post = Post::find($id);
        if ($this->ChechPostType($type) && $this->ChechData($post) && $post->user_id == Auth::user()->id)
        {
            $post_fileds = $this->FilterRequestForPostFields($request,$type);
            $post->update($post_fileds);
            $request->session()->flash('istek_edited' , "İstəyiniz uğurla dəyişdirildi və yoxlamadan keçəndən sonra dərc olunacaq.");
            return back();
        }
        else
        {
            return view('errors.503');
        }
    }

    /***************************
     *   :D Gunelden xatire :D *
     *       Thank You         *
     *         Gunel           *
     * *************************/
    public function deleteAjax(Request $req)
    {
        if($req->ajax()){
            $name = $_POST['imagefile'];
            $he = PostImage::where('photo_name',$name);
            echo $he->count();
            $im_length = $_POST['im_length'];
            if($he->count()==1 && $im_length==1){
                $img_error = "olmaz";
                return json_encode($img_error);
            }else{
                if(file_exists('image/'.$name)){
                    unlink('image/'.$name);
                    $he->delete();
                }
            }
        }
    }

    public function only_pic(Request $req)
    {

        if ($req->ajax()) {

            if($req->imgLength>5){
                return false;
            }

            $file_type = $req->file->getClientOriginalExtension();
            $lowered = strtolower($file_type);

            $check = $this->imageType($req->file);

            if ($check==true) {
                $fileName = $req->file->getClientOriginalName();
                $file = $_FILES['file'];
                $istek_id = $_POST['istek_id'];
                $file['istek_id'] = $istek_id;
                $file_name =date('ygmis').'.'.$fileName;
                $req->file->move(public_path('image'), $file_name);
                $sekil = Post::find($istek_id);
                $hamsi = $sekil->photos();
                $data = new PostImage();
                $data->photo_name = $file_name;
                $hamsi->save($data);
                return json_encode($file_name);
            }else{
                $file_name="error";
                return json_encode($file_name);
            }
        }

    }

    public function delete_post($id)
    {
        $post = Post::find($id);
        if ($this->ChechData($post) && $post->user_id == Auth::user()->id)
        {
            foreach ($post->photos as $photo) {
                unlink('image/'.$photo->photo_name);
            }
            $post->userPost()->delete();
            $post->messages()->delete();
            $post->photos()->delete();
            $post->delete();
            return back();
        }
        else
        {
            return view('errors.404');
        }
    }

    public function get_post_edit($type,$slug)
    {
        $post = Post::where('slug','=',$slug)->first();
        if ($this->ChechPostType($type) && $this->ChechData($post) && $post->user_id ==Auth::user()->id)
        {
            return view('web.post_edit',compact('post'));
        }
        else
        {
            return view('errors.503');
        }
    }

    public function search_post(Request $request)
    {
        if ($request->ajax())
        {
            $location = strtolower($request->city_name);
            $type = $request->post_type;
            if ($location =="all" && $type =="all")
            {
                $posts=Post::select(array('id','title','about','lat','lng','post_type','is_active','slug'))->where('is_active','=','1')->get();
            }
            else if($location !=="all" && $type !=="all")
            {
                $posts=Post::select(array('id','title','about','lat','lng','post_type','is_active','slug'))->where([
                    ['location','LIKE','%'.$location.'%'],
                    ['post_type','LIKE','%'.$type.'%'],
                    ['is_active','=','1']
                ])->get();
            }
            else if ($location !=="all")
            {
                $posts=Post::select(array('id','title','about','lat','lng','post_type','is_active','slug'))->where([
                    ['location','LIKE','%'.$location.'%'],
                    ['is_active','=','1']
                ])->get();
            }
            else if ($location =="all" && $type !=="all")
            {
                $posts=Post::select(array('id','title','about','lat','lng','post_type','is_active','slug'))->where([
                    ['post_type','LIKE','%'.$type.'%'],
                    ['is_active','=','1']
                ])->get();
            }
            foreach ($posts as $key => $value) {
                $posts[$key]['image'] = $value->photos[0]->photo_name;
            }

            return response($posts,200);
        }
    }

    public function imageType($name)
    {
        $file_type = strtolower($name->getClientOriginalExtension());
        if($file_type =='jpg' || $file_type =='jpeg' || $file_type =='png'){

            if($name->getRealPath() && !@is_array(getimagesize($name->getRealPath()))){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
}
