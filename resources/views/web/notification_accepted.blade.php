@extends('layouts.web')
@section('title')
    Bildiriş
@endsection
@section('content')
    <div id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-left">Ismarıclar</h1>
                </div>
            </div>
        </div>
    </div>
    <section id="notification-single">
        @if(isset($notification->id))
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <img src="{{url('/image/'.$notification->post->user->avatar)}}" alt="">
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-12">
                        <h3 class="not-single-title">
                            @if($notification->post->post_type==0)
                                <span class="special-istek">{{$notification->post->user->name}}</span> adlı istifadəçi dəstəyinizi qəbul etdi !
                            @elseif($notification->post->post_type==1)
                                <span class="special-destek">{{$notification->post->user->name}}</span> adlı istifadəçi istəyinizi qəbul etdi
                            @endif
                        </h3>
                        <h4 class="not-single-desc">Email: {{$notification->post->user->email}}</h4>
                        <h4 class="not-single-desc">Şəhər : {{$notification->post->user->city->city_name}}</h4>
                        <h4 class="not-single-desc">Ünvan : {{$notification->post->location}}</h4>
                        <h4 class="not-single-desc">Tel : {{$notification->post->phone}}</h4>
                    </div>
                </div>
            </div>
        @else
            <h1 class="text-center">Sorğunuz düzgün deyil !</h1>
        @endif
    </section>
@endsection