$( document ).ready(function () {
    var auth = $('.auth').val();
    var receiver = $('.receiver').val();
    var post_id = $('.post').val();
    var chat_body = $('.chat-body');
    var message_id = $('.message_id').val();
    var socket_messages_number = $('.socket-messages-number');
    var socket_messages_data = $('.socket-messages-data');
    var socket = io.connect(window.location.protocol+'//'+window.location.hostname+':3000');
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

    // Message Count
    if (localStorage.messageCount)
    {
        var messageCount = localStorage.getItem("messageCount");
        if (messageCount == 0)
        {
            socket_messages_number.html("");
            socket_messages_number.append('<i class="fa fa-comments-o"></i>')
        }
        else
        {
            socket_messages_number.append('<span class="contact-auth-notification-number">'+messageCount+'</span>')
        }
    }
    // Get Messages
    socket_messages_number.on('click',function () {
        $.get('/get-messages/'+messageCount,function (messages) {
            localStorage.messageCount = 0;
            socket_messages_number.html("");
            socket_messages_number.append('<i class="fa fa-comments-o"></i>');
            $('.socket-messages-data').empty();
            if (messages !== 'empty')
            {
                $.each(messages,function (key,value) {
                    appendMessages(value);
                })
            }
            else
            {
                $('.socket-messages-data').append('<li><a href="#"><h4 class="text-center margin0">Mesajınız yoxdur</h4></a></li>');
            }
        })
    });

    // Send Message

    $('#notification_chat').submit(function (e) {
        e.preventDefault(notification_chat);
        var message = $.trim($('.chat-footer-input').val());
        $('.chat-footer-input').val("");
        if (message !== "") {
            $.post('/send-message/'+receiver+'/'+post_id,{send_message:message},function (message) {
                $('.chat-body-message').append(
                    '<li class="pull-right">' +
                    '<p class="chat-message-content">'+String(message.message).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')+'</p>'+
                    '<img src="/image/'+message.sender_attr.avatar+'" class="chat-message-img" alt="user-image">'+
                    '</li>'+
                    '<div class="clearfix"></div>'
                );
            });
        }
    });
    // append message with socket
    socket.on('send_message', function (data) {
        var data = jQuery.parseJSON(data);
        var message = data.data.message;
        if (message.receiver_id == auth)
        {
            if (localStorage.messageCount)
            {
                localStorage.messageCount = Number(localStorage.messageCount) + 1;
            }
            else
            {
                localStorage.setItem("messageCount", 1);
            }
            socket_messages_number.html("");
            socket_messages_number.append('<i class="fa fa-comments-o"></i><span class="count contact-auth-notification-number notification-message">'+localStorage.messageCount+'</span>')
            if (document.location.pathname != '/chat/'+auth+'/'+post_id && document.location.pathname != '/message/'+message_id)
            {
                showMessageWithToast(message);
            }
            else if (message.post_id !== post_id)
            {
                showMessageWithToast(message);
            }
            var a = new Audio("/mp3/message_sound.mp3");
            a.play();
        }
        if (message.sender_id == receiver && message.receiver_id == auth && message.post_id == post_id)
        {
            appendMessageToChat(message);
        }
        chat_body.animate({scrollTop: chat_body.prop("scrollHeight")}, 500);
    });
    chat_body.animate({scrollTop: chat_body.prop("scrollHeight")}, 500);

    //functions
    function appendMessages(value) {
            if (value.receiver_id == auth) {
                socket_messages_data.append(
                    '<li>' +
                    '<a href="/message/'+value.id+'">' +
                    '<img src="/image/' + value.sender_attr.avatar + '" class="img-responsive pull-left" alt="Notification image" />' +
                    '<p>'+ '<span style="color:#0090D9;">' + value.sender_attr.name + ':</span> '+"<br>"+ String(value.message).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;') +'</p></a></li>'
                );
            }
    }

    function appendMessageToChat(message) {
        $('.chat-body ul').append(
            '<li class="pull-left">' +
            '<img src="/image/'+message.sender_attr.avatar+'" class="chat-message-img" alt="user-image">'+
            '<p class="chat-message-content">'+String(message.message).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')+'</p>'+
            '</li>'+
            '<div class="clearfix"></div>'
        );
    }

    function showMessageWithToast(message) {
        iziToast.show({
            title: message.sender_attr.name,
            message: message.message.substring(0,18),
            buttons: [
                ['<button style="width: 55px;height: 40px">Bax</button>', function (instance, toast) {
                    document.location.href = '/message/'+message.id;
                }]
            ],
        });
    }
});