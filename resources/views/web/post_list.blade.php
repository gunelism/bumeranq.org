@extends('layouts.web')
@php
    if (Request::is('post-list/0'))
    {
        $title_and_breadcrumb = 'Bütün istəklər';
        $type = 'İstək';
        $class = 'istek';
    }
    else
    {
        $title_and_breadcrumb = 'Bütün dəstəklər';
        $type = 'Dəstək';
        $class = 'destek';
    }

@endphp
@section('title', $title_and_breadcrumb)

@section('content')
    <div id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-left">{{$title_and_breadcrumb}}</h1>
                </div>
            </div>
        </div>
    </div>
    <section id="news">
        <div class="container">
            @foreach (array_chunk($posts->getCollection()->all(), 4) as $row)
                <div class="row">
                    <div class="col-lg-12">
                    {{-- <div class="col-lg-offset-1"> --}} {{--deyishdirile biler --}}
                    <!-- News block -->
                        @foreach($row as $post)
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 padding0 thumbnail">
                                <div class="news-block">
                                    <div class="news-image col-lg-12 padding0">
                                        <div class="news-type news-{{$class}}">
                                            {{$type}}
                                        </div>
                                        <a href="{{url('/post/'.$post->post_type.'/'.$post->slug)}}"><img src="{{url('/image/' .$post->photos[0]->photo_name)}}" alt="{{$type}} image" /></a>
                                    </div>
                                    <div class="news-content col-lg-12 padding0">
                                        <div class="news-title">
                                            <a href="{{url('/post/'.$post->post_type.'/'.$post->slug)}}">{{$post->title}}</a>
                                        </div>
                                        <div class="news-location col-lg-12">
                                            <p><i class="fa fa-map-marker"></i> {{strlen($post->location) > 45 ? substr($post->location,0,45).' ...' : $post->location}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{-- </div> --}}
                    </div>
                </div>
            @endforeach
            <div class="pull-right">

                {{$posts->links()}}
            </div>
        </div>
        </div>
    </section>
@endsection