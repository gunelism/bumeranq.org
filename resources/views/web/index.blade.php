@extends('layouts.web')

@section('title','Ana Səhifə')
@section('content')
    <section id="map">
        <div class="container-fluid">
            <div class="row">
                <div id="searchBoxDrag">
                    <form id="mapSearch" class="form-inline" method="post">
                        <ul>
                            <li>
                                <label> Şəhər / region</label>
                                <select class="Test" id="cities">
                                    <option name="location" value="all">Hamısı</option>
                                    @foreach($cities as $city)
                                        <option name="location" value="{{$city->city_name}}">{{$city->city_name}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label> İstək / Dəstək</label>
                                <select class="Test" id="post_type">
                                    <option  name="type" value="all">Hamısı</option>
                                    <option name="type" value="1">Dəstək</option>
                                    <option name="type" value="0">İstək</option>
                                </select>

                            </li>
                        </ul>
                        <div class="search-error-desc">
                            <p class="infoMessage text-center"></p>
                        </div>
                    </form>
                </div>
                <img class="Load openLoad closeLoad" src="{{url('images/info-loading.gif')}}">
                <div id="InfoMap">
                </div>
            </div>
            <div class="map-socket-section hidden-xs">
            </div>
        </div>
    </section>
    <section id="news">
        <div class="container">
            <div class="row">
                <div class="news-left col-lg-6 col-md-6 col-sm-12 col-xs-12 padding0">
                    <div class="col-lg-12 news-type-title">
                        <h1>İSTƏKLƏR</h1>
                        <hr>
                    </div>
                <!-- News block -->
                    @foreach ($datas_istek as $data)
                         @if($data->post_type=='0')
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding0 thumbnail">
                            <div class="news-block">
                                <div class="news-image col-lg-12 padding0">
                                    <div class="news-type news-istek">
                                        İstək
                                    </div>
                                    <a href="/post/0/{{$data->slug}}"><img src="{{url('/image/' .$data->photos[0]->photo_name)}}" alt="İstək image" /></a>
                                </div>
                                <div class="news-content col-lg-12 padding0">
                                    <div class="news-title">
                                        <a href="/post/0/{{$data->slug}}">{{$data->title}}</a>
                                    </div>
                                    <div class="news-location col-lg-12">
                                        <p><i class="fa fa-map-marker"></i> {{strlen($data->location) > 45 ? substr($data->location,0,45).' ...' : $data->location}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                         @endif
                    @endforeach

                    <div class="col-lg-12 news-all-isteks-button">
                        <a href="{{url('/post-list/0')}}" class="btn pull-right">Bütün istəklər <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>

                <div class="news-right col-lg-6 col-md-6 col-sm-12 col-xs-12 padding0">
                    <div class="col-lg-12 news-type-title">
                        <h1>DƏSTƏKLƏR</h1>
                        <hr>
                    </div>
                    <!-- News block -->
                    @foreach ($datas_destek as $data)
                         @if($data->post_type=='1')
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding0 thumbnail">
                            <div class="news-block">
                                <div class="news-image col-lg-12 padding0">
                                    <div class="news-type news-destek">
                                        Dəstək
                                    </div>
                                    <a href="/post/1/{{$data->slug}}"><img src="{{url('/image/'.$data->photos[0]->photo_name)}}" alt="Dəstək image" /></a>
                                </div>
                                <div class="news-content col-lg-12 padding0">
                                    <div class="news-title">
                                        <a href="/post/1/{{$data->slug}}">{{$data->title}}</a>
                                    </div>
                                    <div class="news-location col-lg-12">
                                        <p><i class="fa fa-map-marker"></i> {{strlen($data->location) > 45 ? substr($data->location,0,45).' ...' : $data->location}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                     @endif
                @endforeach
                <!-- News block end -->
                    <div class="col-lg-12 news-all-desteks-button">
                        <a href="{{url('/post-list/1')}}" class="btn pull-right">Bütün dəstəklər <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{url('/js/customMap.js')}}" charset="utf-8"></script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAanmTrOlQYWRepobnwqSO1E2SOoHYMRBA&callback=Mydata&language=az" async defer></script>
    <script type="text/javascript">
        //------------------------------For searchBoxDrag -----------------------------------------
        $('#searchBoxDrag').draggable({
            containment: '#InfoMap'
        });
        //------------------------------For searchBoxDrag End -------------------------------------
    </script>
@endsection