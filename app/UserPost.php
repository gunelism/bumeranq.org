<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPost extends Model
{
    protected $fillable = ['user_id','post_id','post','post_user_id','read','accepted','description'];
    protected $appends = ['user','post_user','post_attr'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function getUserAttribute()
    {
        return User::select(array('id','avatar','name'))->where('id',$this->user_id)->first();
    }

    public function getPostUserAttribute()
    {
        return User::select(array('id','avatar','name'))->where('id',$this->post_user_id)->first();
    }

    public function getPostAttrAttribute()
    {
        return Post::select(array('id','post_type'))->where('id',$this->post_id)->first();
    }
}
