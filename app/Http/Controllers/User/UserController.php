<?php

namespace App\Http\Controllers\user;

use App\City;
use App\Http\Requests\UpdateProfilRequest;
use App\Message;
use App\User;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\UserPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class UserController extends Controller
{

    public function profil($type)
    {
        if ($this->ChechPostType($type)){
            $cities = City::all();
            $userPost = UserPost::whereRaw('post_user_id',Auth::user()->id,'OR','user_id',Auth::user()->id)->get();
            $messages = Message::whereRaw("id IN (SELECT MAX(id) FROM messages WHERE receiver_id=".Auth::user()->id." GROUP BY sender_id)")->orderBy('id','desc')->get();
//            dd($messages);
            return view('web.profil',compact('cities','userPost','messages'));
        }
        else
        {
            return view('errors.503');
        }

    }

    public function user_login(LoginRequest $request)
    {
        $auth = false;
        $is_active = true;
        $user = User::where('email',$request->email)->get();
        if ($user[0]->is_active == 0)
        {
            $is_active = false;
        }
        else
        {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => 1 , 'is_admin' => 0]))
            {
                $messageCount = Message::where([['receiver_id',Auth::user()->id],['seen','1']])->count();
                $notificationCount = UserPost::where([['post_user_id',Auth::user()->id],['read','1']])->count();;
                $auth = true;
            }
            else
            {
                $messageCount = 0;
                $notificationCount = 0;
            }
        }
        if ($request->ajax())
        {
            return response()->json([
                'auth' => $auth,
                'previous_url' => URL::previous(),
                "is_active"=> $is_active,
                'message_count' => $messageCount,
                'notification_count' => $notificationCount
            ]);
        }
    }

    public function update_profil(UpdateProfilRequest $request)
    {
        $data = [
            'username' => Auth::user()->username,
            'name' => $request['name'],
            'phone' => '+994'.$request['operator'].$request['phone'],
            'city_id' => $request['city']
        ];
        if ($request->hasFile('avatar'))
        {
            $image_name = $request->file('avatar')->getCLientOriginalName();
            $user_avatar = Auth::user()->avatar;
            if (!$user_avatar == 'prof.png')
            {
                unlink('image/'.$user_avatar);
            }
            $avatar_name =  rand(1,99999).'-'.Auth::user()->username.'.'.$image_name;
            $request->file('avatar')->move(public_path('image/'),$avatar_name);
            $data['avatar'] = $avatar_name;
        }
        else
        {
            $profil_image = Auth::user()->avatar;
            $data['avatar'] = $profil_image;
        }
        Auth::user()->update($data);
        $request->session()->flash('settings','Məlumatlarınız uğurla yeniləndi.');
        return back();
    }
}
