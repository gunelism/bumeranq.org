@extends('layouts.web')
@section('title','Ətraflı')
@section('content')
    <style type="text/css">
        .littleImg{
            margin: 3% 0 0 2%;
            width: 18%;
            border-radius: 5%;
            height: 100px;
            overflow: hidden;
            float: left;
        }

    </style>
    <div id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-left">{{$single_post->title}}</h1>
                </div>
            </div>
        </div>
    </div>
    <section id="single">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-img">
                        <div class="single-img-deadline">
                            <i class="fa fa-calendar"></i>
                            @if(!$diff->d == 0 && $diff->m == 0)
                            {{$diff->d}} gün
                            @elseif(!$diff->y == 0 && !$diff->m == 0 && $diff->d == 0)
                            {{$diff->y}} il {{$diff->m}} ay
                            @elseif (!$diff->y == 0 && $diff->m == 0 && $diff->d == 0)
                            {{$diff->y}} il
                            @elseif ($diff->y == 0 && !$diff->m == 0 && !$diff->d == 0)
                            {{$diff->m}} ay {{$diff->d}} gün
                            @else
                            {{$diff->y}} il {{$diff->m}} ay {{$diff->d}} gün
                            @endif
                        </div>
                        <div class="mainImg">

                            <img src="{{url('/image/'.$single_post->photos[0]->photo_name)}}" class="img-responsive img-single-big" alt="" />
                        </div>
                        <div class="single-img-location">
                            <i class="fa fa-map-marker"></i> {{$single_post->location}}
                        </div>
                        {{-- SLIDER PART --}}

                        @foreach($single_post->photos as $imgName)
                            <div class="littleImg">
                                <img src="{{url('/image/'.$imgName->photo_name)}}" class="img-responsive img-single-small" alt="" />
                            </div>
                        @endforeach
                    </div>
                </div>
                @php
                    $url = 'http://bumeranq.org/post/'.$single_post->post_type.'/'.$single_post->slug;
                @endphp
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-social">
                        <ul class="list-inline">
                            <li class="single-social-facebook faceBook">
                                <div class="social-buttons">
                                    <a href="#"
                                       data-open-share="facebook"
                                       data-open-share-link="{{ $url }}"
                                       data-open-share-picture="{{url('/image/'.$single_post->photos[0]->photo_name)}}"
                                       data-open-share-caption="Bumeranq.org"
                                       data-open-share-description="{{$single_post->about}}"
                                       data-open-share-dynamic="{{$single_post->title}}"

                                       target="_blank">
                                        <i class="fa fa-facebook"></i> PAYLAŞ
                                    </a>
                                    {{--<a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode($url)}}"><i class="fa fa-facebook"></i> PAYLAŞ</a>--}}
                                </div>
                            </li>

                            <li  class="single-social-google" >
                                <div class="social-buttons">
                                    <a href="#"
                                       data-open-share="google"
                                       data-open-share-url="{{ $url }}"
                                       data-open-share-dynamic="{{$single_post->title}}"
                                       target="_blank">
                                        <i class="fa fa-google"></i> PAYLAŞ
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="single-content">
                        <p>
                            {{$single_post->about}}
                        </p>
                    </div>
                    @if(Auth::user())
                        @if (!$check_post)
                            @if(Auth::user()->id != $single_post->user_id)
                                <div class="single-support">
                                    <p class="text-right">
                                    @if($errors->has('description'))
                                        <div class="alert alert-danger" role="alert">{{$errors->first('description')}}</div>
                                    @endif
                                    @if ($single_post->post_type == 0)
                                        @if (Session::has('description_destek'))
                                            <div class="alert alert-success" role="alert">{{Session::get('description_destek')}}</div>
                                        @endif
                                        <a class="btn destek-ol-button" role="button"><i class="fa fa-check"></i> DƏSTƏK OLMAQ İSTƏYİRƏM</a>
                                    @else
                                        @if (Session::has('description_istek'))
                                            <div class="alert alert-success" role="alert">{{Session::get('description_istek')}}</div>
                                        @endif
                                        <a class="btn destek-ol-button" role="button"><i class="fa fa-check"></i> DƏSTƏKDƏN YARARLANMAQ İSTƏYİRƏM</a>
                                    @endif
                                    <div class="alert alert-success destek-ol-message">

                                        <form class="" action="{{url('/notification-add/'.$single_post->post_type.'/'.$single_post->id)}}" method="post">
                                            {{csrf_field()}}
                                            <label for=""><h4>Açıqlama</h4></label>
                                            <textarea name="description" rows="8" cols="80" class="form-control"></textarea>
                                            <input type="submit" name="send" class="pull-right btn" value="Göndər">
                                            <div class="clear-fix"></div>
                                        </form>
                                    </div>
                                    </p>
                                </div>
                            @endif
                        @else
                            @if (Session::has('description_destek'))
                                <div class="alert alert-success" role="alert">{{Session::get('description_destek')}}</div>
                            @elseif (Session::has('description_istek'))
                                <div class="alert alert-success" role="alert">{{Session::get('description_istek')}}</div>
                            @elseif ($single_post->post_type == 1)
                                <div class="alert alert-success" role="alert">Siz artıq bu dəstəkdən yararlanmaq üçün müraciət etmisiniz.</div>
                            @elseif ($single_post->post_type == 0)
                                <div class="alert alert-success" role="alert">Siz artıq bu istəyə dəstək göndərmisiniz.</div>
                            @endif
                        @endif


                    @elseif(Auth::guest())
                        <div class="single-support">
                            <p class="text-right">
                                @if ($single_post->post_type == 0)
                                    <a class="btn destek-ol-button" role="button"><i class="fa fa-check"></i> DƏSTƏK OLMAQ İSTƏYİRƏM</a>
                                @else
                                    <a class="btn destek-ol-button" role="button"><i class="fa fa-check"></i> DƏSTƏKDƏN YARARLANMAQ İSTƏYİRƏM</a>
                                @endif
                            </p>
                        </div>
                        <div class="alert alert-danger destek-ol-message">
                            @if ($single_post->post_type == 0 || $single_post->post_type == 1)
                                <h5 class="text-center">Dəstək olmaq üçün <a href="/register" class="register-color">qeydiyyatdan</a> keçməyiniz və ya <a href="#" data-toggle="modal" data-target="#contact-login-modal"> daxil olmağınız </a> tələb olunur.</h5>
                                 @else
                                <h5 class="text-center">Dəstəkdən yararlanmaq üçün <a href="/register" class="register-color">qeydiyyatdan</a> keçməyiniz tələb olunur .Zəhmət ol<a href="#" data-toggle="modal" data-target="#contact-login-modal"> Daxil ol</a></h5>
                            @endif
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src='https://cdn.rawgit.com/OpenShare/openshare/master/dist/openshare.js'></script>
@endsection
