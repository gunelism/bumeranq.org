$( document ).ready(function() {
    var auth = $('.auth').val();
    var notification_list = $('.notification-list');
    var notification_class = $('.notification');
    var socket = io.connect(window.location.protocol+'//'+window.location.hostname+':3000');

    // notification count
    if (localStorage.notificationCount)
    {
        var notificationCount = localStorage.getItem("notificationCount");
        if (notificationCount == 0)
        {
            notification_class.html("");
            notification_class.append('<i class="fa fa-bell"></i>')
        }
        else
        {
            notification_class.append('<span class="count contact-auth-notification-number">'+notificationCount+'</span>');
        }
    }
    //get notification with socket
    // get notifications
    notification_class.on('click',function () {
        $.get('/get-notifications/'+notificationCount,function (notifications) {
            localStorage.notificationCount = 0;
            notification_class.html("");
            notification_class.append('<i class="fa fa-bell"></i>');
            if (notifications !== 'false')
            {
                notification_list.html('');
                $.each(notifications,function (key,value) {
                    appendNotification(value);
                });
                notification_list.append('<li><a href="/notifications/1"><h4 class="text-center margin0">Hamısına bax</h4></a></li>');
            }
            else
            {
                notification_list.html('');
                notification_list.append('<li><a href="#"><h4 class="text-center margin0">Bildirisiniz yoxdur</h4></a></li>');
            }
        })
    });
    socket.on('notification', function (notificiation_data) {
        var data = jQuery.parseJSON(notificiation_data);
        var notificiation_socket = data.data.notification;
        if (notificiation_socket.post_user_id == auth)
        {
            if (localStorage.notificationCount)
            {
                localStorage.notificationCount = Number(localStorage.notificationCount) + 1;
            }
            else
            {
                localStorage.setItem("notificationCount", 1);
            }
            if ($('.notification-number').is('.notification-number'))
            {
                $('.notification-number').html(localStorage.notificationCount);
            }
            else
            {
                $('.notification').append('<span class="count contact-auth-notification-number notification-number">'+localStorage.notificationCount+'</span>');
            }
            var notification_sound = new Audio('mp3/notification.mp3');
            notification_sound.play();

        }
    });

    function appendNotification(value) {
        var noti_text_els_user= (value.post_attr.post_type == 0) ?'<span class="special-destek">'+ value.user.name +'</span> adlı istifadəçi istəyinizə dəstək vermək istəyir !':
            '<span class="special-istek">'+ value.user.name +'</span> adlı istifadəçi dəstəyinizə istək vermək istəyir !';

        var noti_text_qars_user= (value.post_attr.post_type == 0) ?'<span class="special-istek">'+ value.post_user.name+'</span> adlı istifadəçi dəstəyinizi qəbul etdi !':
            '<span class="special-istek">'+ value.post_user.name +'</span> adlı istifadəçi istəyinizi qəbul etdi !';

        if (value.post_user.id==auth)
        {
            notification_list.append('<li>'+
                '<a href="/notification/'+value.id +'"class="notification-seen">'+
                '<img src="/image/' + value.user.avatar + '" class="img-responsive pull-left" alt="Notification image" />'+
                '<p>'+noti_text_els_user+'</p>'+
                '</a>'+
                '</li>'
            );
        }
        else if(value.user.id==auth && value.accepted == 1) {
            notification_list.append('<li>' +
                '<a href="/notification-accepted/' + value.id + '"class="notification-seen">' +
                '<img src="/image/' + value.post_user.avatar + '" class="img-responsive pull-left" alt="Notification image" />' +
                '<p>' + noti_text_qars_user + '</p>' +
                '</a>' +
                '</li>'
            );
        }
    }
});