$( document ).ready(function () {
    var socket = io.connect(window.location.protocol+'//'+window.location.hostname+':3000');
    var users = $('.users');
    var messenger_body = $('.chat-messenger-body');
    var messenger = $('.messenger');
    var socket_messages_number = $('.socket-messages-number');
    var socket_messages_data = $('.socket-messages-data');
    var auth = messenger.attr('user');
    // Choose user
    users.on('click',function () {
        var name = $(this).find('strong').text();
        messenger.attr('post',$(this).attr('id'));
        messenger.attr('sender',$(this).find('.user-avatar').attr('alt'));
        users.removeClass('user-active');
        $(this).addClass('user-active');
        $('.receiver_name').text(name);
        $.get('/get-messenger-messages/'+messenger.attr('sender')+'/'+messenger.attr('post'),function (messages) {
            messenger_body.html("");
            $.each(messages,function (key,message) {
                appendMessageOnMessenger(message,messenger.attr('sender'));
            })
            $('.chat-body').animate({scrollTop: $('.chat-body').prop("scrollHeight")}, 500);
        });
    });

    // Send Message
    $('#notification_messenger').submit(function (e) {
        e.preventDefault();
        var receiver_id = messenger.attr('sender');
        var post_id = messenger.attr('post');
        var message = $.trim($('.chat-footer-input').val());
        $('.chat-footer-input').val("");
        if (receiver_id !=0 && post_id !=0 && message !== "")
        {
            $.post('/send-message/'+receiver_id+'/'+post_id,{send_message:message},function (message) {
                messenger_body.append(
                    '<li class="pull-right">' +
                    '<p class="chat-message-content">'+String(message.message).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')+'</p>'+
                    '<img src="/image/'+message.sender_attr.avatar+'" class="chat-message-img" alt="user-image">'+
                    '</li>'+
                    '<div class="clearfix"></div>'
                );
            });
        }
        $('.chat-body').animate({scrollTop: $('.chat-body').prop("scrollHeight")}, 500);
    });

    // Socket Messeage

    socket.on('send_message', function (data) {
        var data = jQuery.parseJSON(data);
        var message = data.data.message;
        if (message.receiver_id == auth)
        {
            localStorage.messageCount = Number(localStorage.messageCount) + 1;
            socket_messages_number.html("");
            socket_messages_number.append('<span class="count contact-auth-notification-number notification-message">'+localStorage.messageCount+'</span>')
            if (document.location.pathname !== '/profil/1' && document.location.pathname !== '/my-posts/0' && document.location.pathname !== '/my-posts/1')
            {
                showMessageWithToast(message);
            }
            var a = new Audio("/mp3/message_sound.mp3");
            a.play();
        }
        if (message.sender_id == messenger.attr('sender') && message.receiver_id == auth && message.post_id == messenger.attr('post'))
        {
            appendSocketMessageOnMessenger(message);
        }
        $('.chat-body').animate({scrollTop: $('.chat-body').prop("scrollHeight")}, 500);
    });

    // Search user
    $('.search-user').keyup(function () {
        _this = this;
        $('.chat-body-message li').each(function () {
            if($(this).find('strong').text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
            {
                $(this).hide();
            }
            else
            {
                $(this).show();
            }
        })
    });
    function appendMessageOnMessenger(message,sender_id) {
        if (message.sender_id == sender_id)
        {
            messenger_body.append(
                '<li class="pull-left">' +
                '<img src="/image/'+message.sender_attr.avatar+'" class="chat-message-img" alt="user-image">'+
                '<p class="chat-message-content">'+String(message.message).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')+'</p>'+
                '</li>'+
                '<div class="clearfix"></div>'
            )
        }
        else if (message.sender_id !== sender_id)
        {
            messenger_body.append(
                '<li class="pull-right">' +
                '<p class="chat-message-content">'+String(message.message).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')+'</p>'+
                '<img src="/image/'+message.sender_attr.avatar+'" class="chat-message-img" alt="user-image">'+
                '</li>'+
                '<div class="clearfix"></div>'
            )
        }
    }

    function appendSocketMessageOnMessenger(message) {
        messenger_body.append(
            '<li class="pull-left">' +
            '<img src="/image/'+message.sender_attr.avatar+'" class="chat-message-img" alt="user-image">'+
            '<p class="chat-message-content">'+String(message.message).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')+'</p>'+
            '</li>'+
            '<div class="clearfix"></div>'
        );
    }

    function showMessageWithToast(message) {
        iziToast.show({
            title: message.sender.name,
            message: message.message.substring(0,18),
            buttons: [
                ['<button style="width: 55px;height: 40px">Bax</button>', function (instance, toast) {
                    document.location.href = '/message/'+message.id;
                }]
            ],
        });
    }
});