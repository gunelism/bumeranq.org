<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Torann\LaravelMetaTags\Facades\MetaTag;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // Defaults
        MetaTag::set('description', 'Fərqli istək sahibləri və onlara dəstək verə biləcək insanları bir araya toplayan online platforma.');
        MetaTag::set('image', asset('images/main.png'));

    }

    protected function ChechPostType($type)
    {
        if ($type == "0" || $type == "1")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected function CheckCollectionData($data)
    {
        if (!empty($data[0]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected function ChechData($data)
    {
        if ($data)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected function FilterRequestForPostFields($request,$type)
    {
        $slug = Date('d-m-y'). '-' . str_slug($request->title);
        $request->request->add(['slug' => $slug]);
        $request->request->add(['post_type' => $type]);
        $request->request->add(['is_active' => '0']);
        $request->merge(array('phone' => '+994'.$request->operator.$request->phone));
        return $request->only(['title',"post_type","is_active","lat","lng","location","about","name","phone","email","deadline","slug","organization","type"]);
    }
}
