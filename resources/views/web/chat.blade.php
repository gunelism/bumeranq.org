@extends('layouts.web')
@section('content')
    <div id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-left">Mesaj</h1>
                </div>
            </div>
        </div>
    </div>
    {{--Chat--}}
    @php
        $message_id = 0;
        if (isset($message->receiver_id)) {
        $receiver = $message->receiver_id;
        $message_id = $message->id;
        }
        else if(isset($sender))
        {
        $receiver = $sender;
        }
        if (isset($message->receiver->name)) {
        $header_name = $message->receiver->name;
        }elseif (isset($user->name)) {
        $header_name = $user->name;
        }

if (Session::has('chatdead')) {
    @endphp
    <span class="container help-block">
                <div class="alert alert-warning"><p>{{Session::get('chatdead')}}</p></div>

       </span>
    @php
        }else{

    @endphp

    <section id="socket-chat-main">
        <input type="hidden" class="receiver" value="{{$receiver}}">
        <input type="hidden" class="post" value="{{$postId}}">
        <input type="hidden" class="message_id" value="{{$message_id}}">
        <input type="hidden" class="avatar" value="{{Auth::user()->avatar}}">
        <div id="chat" class="dsp_none">
            <div class="chat-header">
                <h5 class="chat-header-name">{{$header_name}}</h5>
            </div>
            <div class="chat-body">
                <ul class="list-group chat-body-message list-unstyled">
                    @foreach ($messages as $message)
                        @if ($message->sender_id == Auth::user()->id && $message->receiver_id == $receiver)
                            <li class="pull-right">
                                <p class="chat-message-content">{{$message->message}}</p>
                                <img src="/image/{{$message->sender->avatar}}" class="chat-message-img" alt="user-image">
                            </li>
                            <div class="clearfix"></div>
                        @elseif ($message->sender_id == $receiver && $message->receiver_id == Auth::user()->id)
                            <li class="pull-left">
                                <img src="/image/{{$message->sender->avatar}}" class="chat-message-img" alt="user-image">
                                <p class="chat-message-content">{{$message->message}}</p>
                            </li>
                            <div class="clearfix"></div>
                        @endif
                    @endforeach
                </ul>
            </div>

            <div class="chat-footer">
                <form id="notification_chat">
                    <div class="col-lg-10 padding0">
                        <input type="text" class="form-control chat-footer-input" placeholder="Mesajınız">
                    </div>

                    <div class="col-lg-2 padding0">
                        <button type="submit" name="button" class="btn chat-footer-btn"><i class="fa fa-paper-plane-o"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    @php
        }

    @endphp

@endsection