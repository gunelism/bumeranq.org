<?php

namespace App\Http\Controllers\Social;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;

class GoogleController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google+.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        $users = Socialite::driver('google')->user();
        $user = $users->getName();
        $myEmail = $users->getEmail();
        $request->session()->flash('user',$user);
        $request->session()->flash('email',$myEmail);
        return redirect('/register');
    }
}
