<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['sender_id','sender','receiver','receiver_id','post_id','message'];
    protected $appends = ['sender_attr','receiver_attr'];

    public function sender()
    {
        return $this->belongsTo('App\User','sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User','receiver_id');
    }

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function getSenderAttrAttribute()
    {
        return User::select(array('avatar','name'))->where('id',$this->sender_id)->first();
    }

    public function getReceiverAttrAttribute()
    {
       return User::select(array('avatar','name'))->where('id',$this->receiver_id)->first();
    }
}
