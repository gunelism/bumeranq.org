<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = array(
            array('id' => '1','city_name' => 'Ağcabədi'),
            array('id' => '2','city_name' => 'Ağdam'),
            array('id' => '3','city_name' => 'Ağdaş'),
            array('id' => '4','city_name' => 'Ağdərə'),
            array('id' => '5','city_name' => 'Ağstafa'),
            array('id' => '6','city_name' => 'Ağsu'),
            array('id' => '7','city_name' => 'Astara'),
            array('id' => '8','city_name' => 'Bakı'),
            array('id' => '9','city_name' => 'Balakən'),
            array('id' => '10','city_name' => 'Beyləqan'),
            array('id' => '11','city_name' => 'Bərdə'),
            array('id' => '12','city_name' => 'Biləsuvar'),
            array('id' => '13','city_name' => 'Cəbrayıl'),
            array('id' => '14','city_name' => 'Cəlilabad'),
            array('id' => '15','city_name' => 'Culfa'),
            array('id' => '16','city_name' => 'Daşkəsən'),
            array('id' => '17','city_name' => 'Dəliməmmədli	'),
            array('id' => '18','city_name' => 'Füzuli'),
            array('id' => '19','city_name' => 'Gədəbəy'),
            array('id' => '20','city_name' => 'Gəncə'),
            array('id' => '21','city_name' => 'Goranboy'),
            array('id' => '22','city_name' => 'Göyçay'),
            array('id' => '23','city_name' => 'Göygöl'),
            array('id' => '24','city_name' => 'Cəlilabad '),
            array('id' => '25','city_name' => 'Hacıqabul'),
            array('id' => '26','city_name' => 'Xaçmaz'),
            array('id' => '27','city_name' => 'Xankəndi'),
            array('id' => '28','city_name' => 'Xocalı'),
            array('id' => '29','city_name' => 'Xocavənd'),
            array('id' => '30','city_name' => 'Xırdalan'),
            array('id' => '31','city_name' => 'Xızı'),
            array('id' => '32','city_name' => 'İmişli'),
            array('id' => '33','city_name' => 'İsmayıllı'),
            array('id' => '34','city_name' => 'Kəlbəcər'),
            array('id' => '35','city_name' => 'Kürdəmir'),
            array('id' => '36','city_name' => 'Qax'),
            array('id' => '37','city_name' => 'Qazax'),
            array('id' => '38','city_name' => 'Qəbələ'),
            array('id' => '39','city_name' => 'Qobustan'),
            array('id' => '40','city_name' => 'Tovuz'),
            array('id' => '41','city_name' => 'Quba'),
            array('id' => '42','city_name' => 'Qubadlı'),
            array('id' => '43','city_name' => 'Qusar'),
            array('id' => '44','city_name' => 'Laçın'),
            array('id' => '45','city_name' => 'Lerik'),
            array('id' => '46','city_name' => 'Lənkəran'),
            array('id' => '47','city_name' => 'Masallı'),
            array('id' => '48','city_name' => 'Mingəçevir'),
            array('id' => '49','city_name' => 'Naxçıvan '),
            array('id' => '50','city_name' => 'Neftçala'),
            array('id' => '51','city_name' => 'Oğuz'),
            array('id' => '52','city_name' => 'Ordubad'),
            array('id' => '53','city_name' => 'Saatlı'),
            array('id' => '54','city_name' => 'Sabirabad'),
            array('id' => '55','city_name' => 'Salyan'),
            array('id' => '56','city_name' => 'Samux'),
            array('id' => '57','city_name' => 'Siyəzən'),
            array('id' => '58','city_name' => 'Sumqayıt'),
            array('id' => '59','city_name' => 'Şabran'),
            array('id' => '60','city_name' => 'Şamaxı'),
            array('id' => '61','city_name' => 'Şahbuz'),
            array('id' => '62','city_name' => 'Şəki'),
            array('id' => '63','city_name' => 'Şəmkir'),
            array('id' => '64','city_name' => 'Şərur'),
            array('id' => '65','city_name' => 'Şirvan'),
            array('id' => '66','city_name' => 'Şuşa'),
            array('id' => '67','city_name' => 'Tərtər'),
            array('id' => '68','city_name' => 'Ucar'),
            array('id' => '69','city_name' => 'Yardımlı'),
            array('id' => '70','city_name' => 'Yevlax'),
            array('id' => '71','city_name' => 'Zaqatala'),
            array('id' => '72','city_name' => 'Zəngilan'),
            array('id' => '73','city_name' => 'Zərdab')
        );
        foreach ($cities as $city) {
            DB::table('cities')->insert([
                'city_name' => $city['city_name'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
