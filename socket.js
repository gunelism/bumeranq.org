var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');

server.listen(3000,function () {
    console.log('Server listen on 3000');
});
io.on('connection', function (socket) {
    var messageClient= redis.createClient();
    var notificationClient = redis.createClient();
    var postClient = redis.createClient();
    notificationClient.subscribe('notification');
    messageClient.subscribe('send_message');
    postClient.subscribe('post');

    messageClient.on("message", function(channel, data) {
        socket.emit(channel,data);
    });

    notificationClient.on("message", function(channel, data) {
        socket.emit(channel,data);
    });

    postClient.on('message', function (channel,data) {
        socket.emit(channel,data);
    });

    socket.on('disconnect', function() {
        messageClient.quit();
        notificationClient.quit();
        postClient.quit();
    });

});