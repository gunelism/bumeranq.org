<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PostStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check all post status and update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         DB::table('posts')->whereDate('deadline', '<', date('Y-m-d'))->update(['is_active' => '0']);
        $this->info(' post updated');
    }
}
