<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'about',
        'user_id',
        'post_type',
        'is_active',
        'location',
        'lat',
        'lng',
        'name',
        'image',
        'email',
        'phone',
        'email',
        'organization',
        'deadline'
    ];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function photos()
    {
        return $this->hasMany('App\PostImage');
    }

    public function messages()
    {
        return $this->hasMany('App\Message','post_id');
    }

    public function userPost()
    {
        return $this->hasMany('App\UserPost','post_id');
    }
}
