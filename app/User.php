<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','phone','city_id','avatar','is_admin','is_active','verification_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function is_admin()
    {
        return $this->is_admin;
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function verified()
    {
        $this->is_active = 1;
        $this->verification_token = null;
        $this->save();
    }

    public function user_post()
    {
        return $this->hasMany('App\UserPost','user_id');
    }

}
