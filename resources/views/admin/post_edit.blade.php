@extends('layouts.admin')

@section('title','Elan Edit')

@section('content')
    <div class="row">
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        @endif
        <div class="col-lg-12">
            <h1>{{$post->post_type  == 1 ? 'Dəstək' : 'İstək'}} edit:</h1>
            <br>
            <hr>
            <form class="form-group" action="/admin/post-edit/{{$post->id}}" method="post">
                {{ csrf_field() }}
                <h3>Başlıq:</h3>
                <br>
                <input type="text" name="title" class="form-control" value="{{$post->title}}" maxlength="33">
                <br>
                <h3>Açıqlama:</h3>
                <br>
                @if($errors->has('title'))
                    <span>{{$errors->first('title')}}</span>
                @endif
                <textarea name="about" class="form-control" rows="8" cols="80">{{$post->about}}</textarea>
                <br>
                @if($errors->has('about'))
                    <span>{{$errors->first('about')}}</span>
                @endif
                <input type="submit" name="submit" value="Göndər" class="btn btn-success">
            </form>
        </div>
    </div>
@endsection