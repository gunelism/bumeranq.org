<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{
    protected $fillable=['photo_name'];
    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
