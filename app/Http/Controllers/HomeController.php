<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactMail;
use App\UserPost;
use App\Post;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Torann\LaravelMetaTags\Facades\MetaTag;

class HomeController extends Controller
{

    public function index()
    {
        $datas_destek=Post::raw(1)->orderBy('created_at','desc')->whereRaw('`is_active` = 1 AND `post_type` = 1')->take(4)->get();
        $datas_istek=Post::raw(1)->orderBy('created_at','desc')->whereRaw('`is_active` = 1 AND `post_type` = 0')->take(4)->get();
        $cities = City::all();
        return view('web.index',compact('datas_destek', 'datas_istek','cities'));
    }

    public function get_single_post($type,$slug)
    {
        $single_post = Post::where([['is_active', '=', '1'], ['slug', '=', $slug]])->first();
        if ($this->ChechData($single_post) && $this->ChechPostType($type))
        {
            MetaTag::set('title', $single_post->title);
            MetaTag::set('description', $single_post->about);
            MetaTag::set('image', asset('image/'.$single_post->photos[0]->photo_name));
            $check_post = $this->CheckPostUserData($single_post);
            $date = $single_post->deadline;
            $dbdate= new DateTime($date);
            $newdate= new DateTime('now');
            $diff = date_diff($newdate,$dbdate);
            return view('web.single',compact('single_post','check_post','diff'));
        }
        else
        {
            return view('errors.503');
        }
    }

    public function get_post_list($type)
    {
        if ($this->ChechPostType($type))
        {
            $posts=Post::raw(1)->orderBy('created_at','desc')->whereRaw('`is_active` = 1 AND `post_type` = '.$type)->paginate(8);
            return view('web.post_list',compact('posts'));
        }

    }

    public function send_contact_mail(ContactRequest $request)
    {
        Mail::to('farid.b@code.edu.az')->send(new ContactMail($request->all()));
        $request->session()->flash('send', 'İsmarıcınız müvəffəqiyyətlə göndərildi.');
        return back();
    }

    public function about()
    {
        return view('web.about');
    }

    public function contact()
    {
        return view('web.contact');
    }

    public function post_add($type)
    {
        if ($this->ChechPostType($type))
        {
            return view('web.post_add');
        }
        else
        {
            return view('errors.503');
        }
    }

    public function get_post_user($slug)
    {
        $post = Post::where('slug',$slug)->first();
        if ($this->ChechData($post))
        {
            return view('web.post_user',compact('post'));
        }
    }

    public function CheckPostUserData($post)
    {
        if (Auth::user()) {
            $data = UserPost::where([['user_id', '=', Auth::user()->id], ['post_id', '=', $post->id]])->get();
            if ($this->CheckCollectionData($data)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}