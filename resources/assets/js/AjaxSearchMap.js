$(document).ready(function() {
    var socket = io.connect(window.location.protocol+'//'+window.location.hostname+':3000');

    socket.on('post',function (post_field) {
        var data = jQuery.parseJSON(post_field);
        var post = data.data.post;
        if (post.post_type == 0)
        {
            $('.map-socket-section').prepend("<a href='/post/"+post.post_type+"/"+post.slug+"'>" + "<div class='map-socket-data'>" +  "<span style='color:#0AA699'>" + post.title.substring(0,16) + "...</span>" + " adlı yeni istək əlavə olundu !" + "</div>"+ "</a>");
        }
        else if(post.post_type == 1)
        {
            $('.map-socket-section').prepend("<a href='/post/"+post.post_type+"/"+post.slug+"'>" + "<div class='map-socket-data'>" + "<span style='color:#F35958'>" + post.title.substring(0,16) + "...</span>" + " adlı yeni dəstək əlavə olundu !" + "</div>"+ "</a>");
        }
    });


var markers = [];
$.ajax({
  url: '/search-post',
  type: 'post',
  dataType: 'json',
  data: {
    ElanLocation : $('#Loc').val(),
    ElanType : $('#Type').val(),
  },
  success: function Mydatas(data){
      $('.search-error-desc').hide('2000');
      $('.infoMessage').text('');
      var count = data.length;
    Mydata(data,count);
      $.each(data,function(key,value)
      {
          if (value.post_type == 0)
          {
              $('.map-socket-section').prepend("<a href='/post/"+value.post_type+"/"+value.slug+"'>" + "<div class='map-socket-data'>" +  "<span style='color:#0AA699'>" + value.title.substring(0,16) + "...</span>" + " adlı yeni istək əlavə olundu !" + "</div>"+ "</a>");
          }
          else if(value.post_type == 1)
          {
              $('.map-socket-section').prepend("<a href='/post/"+value.post_type+"/"+value.slug+"'>" + "<div class='map-socket-data'>" + "<span style='color:#F35958'>" + value.title.substring(0,16) + "...</span>" + " adlı yeni dəstək əlavə olundu !" + "</div>"+ "</a>");
          }
      });
  },
  beforeSend:function(){
    $('.Load').removeClass('closeLoad');
  },
  complete:function(){
    $('.Load').addClass('closeLoad');
  },
});
    // <- ============ CHANGE FUNC FOR MAP WITH AJAX =========== ->
  $('.Test').change(function(event) {
    event.preventDefault();
    $.ajax({
      url: '/search-post',
      type: 'POST',
      dataType: 'json',
      data: {
        city_name : $('#cities').val(),
        post_type : $('#post_type').val(),
      },
      success:function Mydatas(data){
          if (data.length ==0) {
            $('.search-error-desc').fadeIn('2000');
            $('.infoMessage').text('İstəyinizə uyğun nəticə tapılmadı')
            $('.search-error-desc').css('background', '#F44336');
            Mydata(data);
          }else {
            var counts = 0;
            for (var i =  0; i < data.length; i++) {
              if (data[i]['is_active'] == 1) {
                counts++;
              }
            }
              $('.search-error-desc').fadeIn('2000');
              $('.infoMessage').text('İstəyinizə uyğun '+counts+' nəticə tapıldi')
              $('.search-error-desc').css('background', '#4CAF50');
              var count = data.length;
              Mydata(data,count);
          }
      },
      beforeSend:function(){
        $('.Load').removeClass('closeLoad');
      },
      complete:function(){
        $('.Load').addClass('closeLoad');
      },
    });
  });
});

  //Index.blade.php Map function
function Mydata(data,count){
  markers = [];
  var myLatlng = new google.maps.LatLng(40.300,48.800);
         var mapOptions = {
             zoom: 8,
             center: myLatlng,
             scrollwheel: false,
             streetViewControl:false,
             mapTypeControl:false,
             overViewMapControl:false
         };
         var map = new google.maps.Map(document.getElementById('InfoMap'), mapOptions);
         var marker;
         for ( i = 0; i < count; i++) {
           if (data[i]['is_active'] == 1) {
             var src;
             if( (data[i]['post_type']) == 0) {
               src= "/images/green-icon.png";
             }else if (data[i]['post_type'] == 1) {
               src="/images/red-icon.png";
             };
             var MyData =data[i]['about'];
             var about = MyData.substring(0,185);
             marker = new google.maps.Marker({
               position: new google.maps.LatLng(data[i]['lat'],data[i]['lng']),
               map: map,
               title: data[i]['title'],
               content:"<div id='infow'>" +
               '<div class="infow-content">' +
               "<a href='/post/"+data[i]['post_type']+"/"+data[i]['slug']+"'><img src='image/"+data[i]['image']+"'height='127' width='140'></a>" +
               '<div class="infoBubble-desc">' +
               "<p style='padding-right:9px'>"+about+" ...</p>"+
               '</div>' +
               "<p class='pull-right '><a href='/post/"+data[i]['post_type']+"/"+data[i]['slug']+"' class='btn infoBubble-button'>Ətraflı</a></p>"+
               '</div>' +
               '</div>',
               animation: google.maps.Animation.DROP,
               icon:src,
             });
             markers.push(marker);
           }
         };
         var infoBubble2 = new InfoBubble({
             map: map,
             shadowStyle: 1,
             padding: 0,
             backgroundColor: 'white',
             borderRadius: 0,
             arrowSize: 15,
             minWidth:350,
             maxWidth: 350,
             minHeight:127,
             maxHeight: 127,
             borderWidth: 0,
             borderColor: '#2c2c2c',
             disableAutoPan: false,
             hideCloseButton: false,
             arrowPosition: 50,
             backgroundClassName: 'InfoMap',
             arrowStyle: 2,
             closeSrc: '/images/icon.png',
           });
            function manyInfo(mark, infoBubble2) {
            infoBubble2.setContent(mark.content);
            $('.InfoMap').parent().prev().addClass('InfoBubble-close');
            infoBubble2.open(map, mark);
            marker.addListener('closeclick', function() {
                infoBubble2.setMarker(null);
            });
          }
         for (var i = 0; i < markers.length; i++) {
             google.maps.event.addListener(markers[i], 'click', function() {
               map.setZoom(13);
                 manyInfo(this, infoBubble2);
             });
         }
         var markerCluster = new MarkerClusterer(map, markers, {
           imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
          maxZoom:8
       });
}
