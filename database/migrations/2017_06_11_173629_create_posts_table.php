<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->char('title');
            $table->char('slug');
            $table->text('about');
            $table->integer('user_id')->unsigned()->index();
            $table->boolean('post_type');
            $table->boolean('is_active');
            $table->string('location');
            $table->string('lat');
            $table->string('lng');
            $table->char('name');
            $table->string('image');
            $table->string('email');
            $table->string('phone');
            $table->char('organization');
            $table->char('type');
            $table->date('deadline');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
