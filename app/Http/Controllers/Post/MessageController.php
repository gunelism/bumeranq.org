<?php

namespace App\Http\Controllers\Post;

use App\Events\CreateMessageEvent;
use App\Message;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function get_messages(Request $request,$count)
    {
        $messages = Message::whereRaw("id IN (SELECT MAX(id) FROM messages WHERE seen=0 AND receiver_id=".Auth::user()->id." GROUP BY sender_id)")->orderBy('id','desc')->get();
        if ($count !== 0)
        {
            foreach ($messages as $message) {
                $message->seen = 0;
                $message->update();
            }
        }
        if ($this->CheckCollectionData($messages) && $request->ajax())
        {
            return $messages;
        }
        else
        {
            return 'empty';
        }
    }

    public function get_messenger_messages($sender_id,$post_id,Request $request)
    {
        $messages = Message::whereRaw('sender_id = '.$sender_id.' AND receiver_id = '.Auth::user()->id.' OR sender_id = '.Auth::user()->id.' AND receiver_id = '.$sender_id.' AND post_id = '.$post_id)->get();
        if ($request->ajax() && $this->CheckCollectionData($messages))
        {
            return $messages;
        }
    }

    public function send_message($receiver,$postId,Request $request,Message $message)
    {
        if ($request->ajax())
        {
            $message->sender_id = Auth::user()->id;
            $message->receiver_id = $receiver;
            $message->post_id = $postId;
            $message->message = $request->send_message;
            $message->seen = 0;
            $message->save();
            event(new CreateMessageEvent($message));
            return $message;
        }
    }

    public function get_all_messages($id,Request $request)
    {
        $message = Message::find($id);
        $post = Post::find($message->post_id);
        $postId = $message->post_id;
        if ($this->ChechData($message) && $this->ChechData($post) && $message->receiver_id == Auth::user()->id)
        {
            $receiver = $message->receiver_id;
            $message->receiver_id = $message->sender_id;
            $message->sender_id = $receiver;
            $messages = Message::where([
                                        ['sender_id', '=', Auth::user()->id],
                                        ['receiver_id', '=',$message->receiver_id],
                                        ['post_id', '=',$message->post_id]
                                    ])
                                ->orWhere([
                                        ['receiver_id', '=', Auth::user()->id],
                                        ['sender_id', '=',$message->receiver_id],
                                        ['post_id', '=',$message->post_id]
                                ])->get();
            if ($post->is_active == 0)
            {
                if ($post->user_id == Auth::user()->id)
                {
                    $request->session()->flash('chatdead', 'Bu elana qoyulan vaxt bitdiyindən elan üzərindən əlaqə sona çatmışdır. Elanın vaxtını uzada bilərsiz');
                }
                else
                {
                    $request->session()->flash('chatdead', 'Bu elana qoyulan vaxt bitdiyindən elan üzərindən əlaqə sona çatmışdır.');
                }
                return view('web.chat');
            }
            return view('web.chat',compact('messages','message','postId'));
        }
        else
        {
            return back();
        }
    }

    public function chat($sender,$postId)
    {
        $user = User::find($sender);
        $post = Post::find($postId);
        $messages = Message::where([
                                    ['sender_id', '=', Auth::user()->id],
                                    ['receiver_id', '=',$sender],
                                    ['post_id', '=',$postId]
                                ])
                            ->orWhere([
                                    ['receiver_id', '=', Auth::user()->id],
                                    ['sender_id', '=',$sender],
                                    ['post_id', '=',$postId]
                            ])->orderBy('id','asc')->get();
        if ($this->ChechData($user) && $this->ChechData($post))
        {
            if ($post->is_active == 0)
            {
                if ($post->user_id == Auth::user()->id)
                {
                    $request->session()->flash('chatdead', 'Bu elana qoyulan vaxt bitdiyindən elan üzərindən əlaqə sona çatmışdır. Elanın vaxtını uzada bilərsiz');
                }
                else
                {
                    $request->session()->flash('chatdead', 'Bu elana qoyulan vaxt bitdiyindən elan üzərindən əlaqə sona çatmışdır.');
                }
                return view('web.chat');
            }
            return view('web.chat',compact('messages','sender','postId','user'));
        }
        else
        {
            return view('errors.503');
        }
    }
}
