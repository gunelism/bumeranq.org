<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'avatar' => 'image'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Ad və Soyadı boş buraxmayın.',
            'phone.required' => 'Əlaqə nömrəsini boş saxlamayın.',
            'city.required' => 'Ünvanı boş saxlamayın.',
            'avatar.image' => 'Yüklədiyiniz faylın formatı şəkil deyil.',
        ];
    }
}
