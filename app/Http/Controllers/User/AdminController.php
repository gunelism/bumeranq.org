<?php

namespace App\Http\Controllers\User;

use App\Events\PublicShowPostEvent;
use App\Http\Requests\AdminLoginRequest;
use App\Http\Requests\AdminPostEditRequest;
use App\Post;
use App\User;
use App\Http\Controllers\Controller;
use App\UserPost;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        $user = User::orderBy('created_at','desc');
        $users = $user->paginate(8);
        $user_count = User::count();
        $admin_count = User::where('is_admin','=',1)->get()->count();
        $istek_count = Post::where('post_type', 0)->get()->count();
        $destek_count = Post::where('post_type', 1)->get()->count();

        return view('admin.index',compact('users','istek_count','destek_count', 'user_count','admin_count'));
        return view('admin.index');
    }

    public function post_list($type)
    {
        if ($this->ChechPostType($type))
        {
            $posts = Post::where('post_type',$type)->orderBy('created_at','desc')->paginate(8);
            return view('admin.post_list',compact('posts'));
        }
        else
        {
            return view('errors.503');
        }
    }

    public function user_post()
    {
        $userPosts = UserPost::paginate(10);
        $acceptedCount = UserPost::where('accepted', 1)->count();
        $refusalCount = UserPost::where('accepted', 0)->count();
        return view('admin.user_post',compact('userPosts','acceptedCount','refusalCount'));
    }

    public function active_actions($id)
    {
        $post = Post::find($id);
        if ($this->ChechData($post))
        {
            if ($post->is_active == 0)
            {
                $status_number = 1;
                event(new PublicShowPostEvent($post));
            }
            else
            {
                $status_number = 0;
            }
            $post->is_active= $status_number;
            $post->update();
            return back();
        }
        else
        {
            return view('errors.503');
        }
    }

    public function get_post_edit($id)
    {
        $post = Post::find($id);
        if ($this->ChechData($post))
        {
            return view('admin.post_edit',compact('post'));
        }
        else
        {
            return view('errors.503');
        }
    }

    public function post_edit($id, AdminPostEditRequest $request)
    {
        $post = Post::find($id);
        if ($this->ChechData($post))
        {
            $post->title = $request->title;
            $post->about = $request->about;
            $request->session()->flash('success','Elan uğurla yeniləndi');
            $post->update();
            return back();
        }
        else
        {
            return view('errors.503');
        }
    }

    public function get_login()
    {
        return view('admin.login');
    }

    public function post_login(AdminLoginRequest $request)
    {
        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => 1,'is_admin' => 1]))
        {
            return redirect('/alfagen');
        }
        else
        {
            $request->session()->flash('admin_login_error','Email və ya şifrə yalnışdır');
            return redirect('/alfagen/login');
        }
    }

    public function access_user($status,$id)
    {
        $user = User::find($id);
        if (Auth::user()->email == "farid.b@code.edu.az" && $this->ChechData($user))
        {
            if ($status == 0)
            {
                $user->is_admin = 1;
            }
            else
            {
                $user->is_admin = 0;
            }
            $user->update();
            return back();
        }
        else
        {
            return view('errors.404');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/alfagen/login');
    }
}
