<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// <==================Page Routes ====================>

Route::get('/','HomeController@index');
Route::get('/about','HomeController@about');
Route::get('/contact','HomeController@contact');
Route::post('/contact','HomeController@send_contact_mail');
Route::get('/home', 'HomeController@index');
Route::post('/search-post', 'Post\PostController@search_post');
Route::get('/post-add/{type}','HomeController@post_add');
Route::get('/post-list/{type}','HomeController@get_post_list');
Route::get('/post/{type}/{slug}','HomeController@get_single_post');
// <================ User Routes =====================>
Route::group(['middleware' => 'auth'],function(){
    //-------------------- User Route ----------------------
    Route::get('/profil/{type}','User\UserController@profil');
    Route::get('/my-posts/{type}','User\UserController@profil');
    Route::get('/settings/{type}','User\UserController@profil');
    Route::get('/notifications/{type}','User\UserController@profil');
    Route::post('/profile-update','User\UserController@update_profil');
    //-------------------- User Route End----------------------

    //-------------------- Post Route----------------------
    Route::get('/post-edit/{type}/{slug}','Post\PostController@get_post_edit');
    Route::get('/post-delete/{id}','Post\PostController@delete_post');
    Route::get('/post-user/{slug}','HomeController@get_post_user');
    Route::post('/post-add/{type}','Post\PostController@create_post');
    Route::post('/deleteAjax','Post\PostController@deleteAjax');
    Route::post('/add_file_change','Post\PostController@only_pic');
    Route::patch('/post-update/{type}/{id}','Post\PostController@update_post');
    //-------------------- Post Route End----------------------

    //-------------------- Message Route----------------------
    Route::get('/get-messages/{count}','Post\MessageController@get_messages');
    Route::get('/message/{id}','Post\MessageController@get_all_messages');
    Route::get('/chat/{sender}/{postId}','Post\MessageController@chat');
    Route::get('/get-messenger-messages/{sender_id}/{post_id}','Post\MessageController@get_messenger_messages');
    Route::post('/send-message/{receiver}/{postId}','Post\MessageController@send_message');
    //-------------------- Message Route End----------------------

    //-------------------- Notification Route----------------------
    Route::get('/get-notifications/{count}','Post\NotificationController@get_notifications');
    Route::get('/notification/{id}','Post\NotificationController@notification');
    Route::get('/notification-accepted/{id}','Post\NotificationController@notification_accepted');
    Route::get('/notification-refusal/{id}','Post\NotificationController@notification_refusal');
    Route::get('/accept/{id}','Post\NotificationController@accept');
    Route::post('notification-add/{type}/{id}','Post\NotificationController@add_notification');
    //-------------------- Notification Route End----------------------
});
Route::post('/user-login','User\UserController@user_login');
//<=================Admin Routes =====================>
Route::get('/alfagen/login','User\AdminController@get_login');
Route::post('/alfagen/post-login','User\AdminController@post_login');
Route::group(['middleware' => 'admin'],function (){
    Route::get('/alfagen','User\AdminController@index');
    Route::get('/admin/logout','User\AdminController@logout');
    Route::get('/admin/access/{status}/{id}','User\AdminController@access_user');
    Route::get('/active-actions/{id}','User\AdminController@active_actions');
    Route::get('/admin/post-list/{type}','User\AdminController@post_list');
    Route::get('/admin/post-edit/{id}','User\AdminController@get_post_edit');
    Route::get('/admin/user-post','User\AdminController@user_post');
    Route::post('/admin/post-edit/{id}','User\AdminController@post_edit');
});
Auth::routes();
Route::get('register/verify/{token}', 'Auth\RegisterController@verify');

//<=================Social Routes =====================>
Route::get('auth/google', 'Social\GoogleController@redirectToProvider')->name('google.login');
Route::get('auth/google/callback', 'Social\GoogleController@handleProviderCallback');

Route::get('facebook', 'Social\FacebookController@redirectToProvider')->name('facebook.login');
Route::get('facebook/callback', 'Social\FacebookController@handleProviderCallback');